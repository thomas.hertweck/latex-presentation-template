# Author: Dr. Thomas Hertweck, Thomas.Hertweck@kit.edu

.SUFFIXES:
.PHONY: FORCE help all pdf force mrproper clean distclean
SHELL = /bin/bash

engine := lua          # lua or pdf or xe
master := exampleKIT

###############################################################
# There is usually no need to change anything below this line #
###############################################################

export TEXINPUTS:=::./:~/texmf/:./texmf/:${TEXINPUTS}

ltxmkopt := -pdf
ifeq ($(strip $(engine)),lua)
  ltxmkopt := -pdflua
endif
ifeq ($(strip $(engine)),xe)
  ltxmkopt := -pdfxe
endif
run := latexmk $(ltxmkopt) -bibtex-cond1 -recorder
tmp := $(master).nav $(master).snm $(master).vrb *.fls *.b??-SAVE-ERROR

help:
	@echo
	@echo -e "Stem of master source: \e[96m$(master)\e[0m"
	@echo "LaTeX engine: $(strip $(engine))latex"
	@echo 
	@echo "$(MAKE) all       - compile LaTeX source into PDF"
	@echo "$(MAKE) force     - force compilation run"
	@echo "$(MAKE) clean     - remove temporary files"
	@echo "$(MAKE) distclean - remove all reproducible files"
	@echo

all: $(master).pdf

pdf: all

$(master).pdf: FORCE
	$(run) $(master)

force: FORCE
	@$(run) -g $(master)

mrproper:
	@rm -f *~

clean: mrproper
	@rm -f $(tmp)
	@$(run) -c -silent $(master)

distclean: mrproper
	@rm -f $(tmp)
	@$(run) -C -silent $(master)

