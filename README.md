## Name
Presentation Template

## Description
A LaTeX template for presentations in KIT design with optional enhancements 
meant for the Applied Geophysics group, Geophysical Institute, KIT. The latest 
version of this template is compatible with the KIT design published in
February 2025. For Powerpoint templates, please visit the KIT intranet.

## Installation
Simply download or clone this repository and start using the template.

A texmf-tree is provided which the included Makefile (or the underlying 
latexmk command) will pick up automatically. You can, however, also install 
the relevant files in your local standard texmf-tree (either system-wide or 
in your home directory). As normal user, copy the texmf-directory to your 
home directory and run "texhash $HOME/texmf" in a shell - under many Linux 
distributions, this should be sufficient. Afterwards, the class should be 
usable from integrated LaTeX editors or any terminal. For other operating 
systems, please proceed accordingly.

## Usage
The presentation template is based on the standard beamer class originally 
developed by Till Tantau and adapted to KIT's publication guidelines by
Thomas Hertweck.

An example LaTeX file is included in this distribution. If you compile the 
example and it finds the beamerKIT class but fails with an error message, 
your TeX distribution might be too old. If a compilation run fails with 
the beamerKIT class not being found, please check your installation, in 
particular the TEXINPUTS shell variable.

Detailed explanations of all available options and macros are given in the
example LaTeX file called exampleKIT.tex.

The class knows about the following options which are described in detail 
below and in the example LaTeX file:

aspectratio = (string) 169 (default) | 1610 | 43; presentations for 
widescreen projectors should use the default (169).

english | german = (string) english (default); the language option sets 
KIT text elements accordingly; this option is also used for package babel 
to get correct language-specific behavior.

largefont = (boolean) false (default) | true; the new official 2025 
presentation layout by KIT has a very small default font size for standard 
text; when setting this option to true, a larger default font size is used.

plain = (boolean) false (default) | true; if this option is given, all 
slides are set in plain mode with the footline completely disabled; it also 
means no progressbar or license icon are shown, even if these options are 
given.

showtotal = (boolean) false (default) | true; if this option is given, not 
only the current slide number but also the total number of slides is shown 
in the footline; for obvious reasons, it makes no sense to combine this 
option with option "plain".

license = (string) none (default) | ccby | ccbysa | ccbynd | ccbync | 
ccbyncsa | ccbyncnd; the license under which the document is published; none 
shows no license information (default). Other available options include 
various Creative Commons licenses such as Attribution (ccby), Attribution - 
ShareAlike (ccbysa), Attribution - NoDerivs (ccbynd), Attribution - 
NonCommerical (ccbync), Attribution - NonCommercial - ShareAlike (ccbyncsa) 
and Attribution - NonCommercial - NoDerivs (ccbyncnd). Attribution - 
ShareAlike or Attribution - NonCommercial - ShareAlike seem good options for 
scientific material, of course dependent on the context of your presentation.

footlic = (boolean) false (default) | true; if this option is given, a small 
license logo is shown in the footer of each slide. This option is ignored if 
license is "none". For obvious reasons, it makes no sense to combine this 
option with option "plain".

progressbar = (boolean) false (default) | true; if this option is given, a 
visual small progressbar is shown in the footline at the bottom of each slide.

design = (string) kit (default) | gpi; if set to gpi, make minor design 
modifications, in particular to the table of contents page. Also sets the 
default for the end page to the GPI design.

colorscheme = (string) iceblue (default) | white; when colorscheme is set to
white, certain elements that normally use color 'iceblue' are displayed in
white to increase the contrast.

grid = (boolean) false (default) | true; when set to true, this option 
provides a grid (spaced at 1 cm) in the background canvas to help align 
elements or determine absolute coordinates for textpos.

## Support
If technical issues are encountered when using this template, or if there are 
suggestions or ideas for improvement, you can get in touch with the author at 
Thomas.Hertweck@kit.edu. Positive feedback, in particular if you find this
LaTeX template useful, is of course also welcome.

## License
(c)2017-2025. This LaTeX template is licensed under a CC BY-NC 4.0 license.
